/**
 * Copyright (c) 2017-present, Facebook, Inc.
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

const React = require('react');

const CompLibrary = require('../../core/CompLibrary.js');

const Container = CompLibrary.Container;
const GridBlock = CompLibrary.GridBlock;

function Help(props) {
  const {config: siteConfig, language = ''} = props;
  const {baseUrl, docsUrl} = siteConfig;
  const docsPart = `${docsUrl ? `${docsUrl}/` : ''}`;
  const langPart = `${language ? `${language}/` : ''}`;
  const docUrl = doc => `${baseUrl}${docsPart}${langPart}${doc}`;

  const supportLinks = [
    {
      content: `Learn more using the [documentation on this site.](${docUrl(
        'introduction.html',
      )})`,
      title: 'Browse Docs',
    },
    {
      content: `Find out what's new with this project in the [Changelog.](https://gitlab.com/creare-com/tabsint/blob/master/CHANGELOG.md)`,
      title: 'Stay up to date',
    },
    {
      content: `Search the [Issue Tracker](https://gitlab.com/creare-com/tabsint/issues)`,
      title: 'Issue Tracker',
    }
  ];

  return (
    <div className="docMainWrapper wrapper">
      <Container className="mainContainer documentContainer postContainer">
        <div className="post">
          <header className="postHeader">
            <h1>About</h1>
          </header>
          <br/>
          <br/>
          <a href="https://creare.com">
            <img src={`${baseUrl}img/creare-logo.jpg`} width="200px" />
          </a>
          <p>This project is developed and maintained by <a href="https://creare.com">Creare</a>
          <br/>
          <br/>
          <a href="mailto:tabsint@creare.com">Contact Us</a> for more information about TabSINT and other related mobile health R&D projects
          </p>
        </div>
        <hr/>
        <div className="post">
          <header className="postHeader">
            <h1>Need help?</h1>
          </header>
          <GridBlock contents={supportLinks} layout="threeColumn" />
        </div>
      </Container>
    </div>
  );
}

module.exports = Help;
