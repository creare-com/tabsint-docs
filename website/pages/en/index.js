/**
 * Copyright (c) 2017-present, Facebook, Inc.
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

const React = require('react');

const CompLibrary = require('../../core/CompLibrary.js');

const MarkdownBlock = CompLibrary.MarkdownBlock; /* Used to read markdown */
const Container = CompLibrary.Container;
const GridBlock = CompLibrary.GridBlock;

class HomeSplash extends React.Component {
  render() {
    const {siteConfig, language = ''} = this.props;
    const {baseUrl, docsUrl} = siteConfig;
    const docsPart = `${docsUrl ? `${docsUrl}/` : ''}`;
    const langPart = `${language ? `${language}/` : ''}`;
    const docUrl = doc => `${baseUrl}${docsPart}${langPart}${doc}`;

    const SplashContainer = props => (
      <div className="homeContainer">
        <div className="homeSplashFade">
          <div className="wrapper homeWrapper">{props.children}</div>
        </div>
      </div>
    );

    const Logo = props => (
      <div className="projectLogo">
        <img src={props.img_src} alt="Project Logo" />
      </div>
    );

    const ProjectTitle = () => (
      <h2 className="projectTitle">
        {siteConfig.title}
        <small>{siteConfig.tagline[0]}<br/>{siteConfig.tagline[1]}</small>
      </h2>
    );

    const PromoSection = props => (
      <div className="section promoSection">
        <div className="promoRow">
          <div className="pluginRowBlock">{props.children}</div>
        </div>
      </div>
    );

    const Button = props => (
      <div className="pluginWrapper buttonWrapper">
        <a className="button" href={props.href} target={props.target}>
          {props.children}
        </a>
      </div>
    );

    return (
      <SplashContainer>
        <Logo img_src={`${baseUrl}img/tabsint.svg`} />
        <div className="inner">
          <ProjectTitle siteConfig={siteConfig} />
          <PromoSection>
            <Button href={`${docUrl('quick-start/tabsint')}`}>Get Started</Button>
          </PromoSection>
        </div>
      </SplashContainer>
    );
  }
}

class Index extends React.Component {
  render() {
    const {config: siteConfig, language = ''} = this.props;
    const {baseUrl} = siteConfig;

    const Block = props => (
      <Container
        padding={['bottom', 'top']}
        id={props.id}
        background={props.background}>
        <GridBlock
          align="center"
          contents={props.children}
          layout={props.layout}
        />
      </Container>
    );

    const FeatureCallout = () => (
      <div
        className="productShowcaseSection paddingBottom"
        style={{textAlign: 'center'}}>
        <h2> </h2>
        <MarkdownBlock> </MarkdownBlock>
      </div>
    );

    const HowToCite = () => (
      <Block background="light">
        {[
          {
            content: 'Use the following citation to attribute TabSINT and WAHTS headset in publications: <br><br><br> Shapiro, M. L., Norris, J. A., Wilbur, J. C., Brungart D. S., & Clavier, O. H. (2020). *TabSINT: Open-Source Mobile Software for Distributed Studies of Hearing*. International Journal of Audiology, 59(sup1), S12-S19. <br><br><br> See [References](../../../docs/references/references.html) for more information.',
            title: 'How to Cite',
          },
        ]}
      </Block>
    );

    const License = () => (
      <Block background="dark">
        {[
          {
            content:
              'Code and documentation Copyright (C) 2015-2021 Creare. Code released under the Apache v2 License, provided in [LICENSE](https://gitlab.com/creare-com/tabsint/blob/master/LICENSE). All rights reserved. <br><br>License agreements for open source libraries used in the project can be found in [NOTICE](https://gitlab.com/creare-com/tabsint/blob/master/NOTICE.md). <br><br>Creare has used commercially reasonable efforts in preparing the TabSINT Software but makes no guarantee or warranty of any nature with regard to its use, performance, or operation. <br><br>Creare makes no representations or warranties, and Creare shall incur no liability or other obligation of any nature whatsoever to any person from any and all actions arising from the use of this software.  THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE HEREBY EXPRESSLY EXCLUDED. The final responsibility for the proper use and functioning of the TabSINT Software shall rest solely with the USAMRAA.',
            title: 'License',
          },
        ]}
      </Block>
    );

    const Awknowledge = () => (
      <Block background="light">
        {[
          {
            content: 'This code was developed and maintained with funding support from the US Army Medical Research Materiel Command and the Army Public Health Command under SBIR Phase III Awards #W81XWH-13-C-0194, W81XWH-16-C-0160, W81XWH-17-C-0218, W81XWH-19-C-0175, W81XWH-20-C-0070, W81XWH-21-C-0082 to [Creare LLC](http://www.creare.com). In particular, we gratefully acknowledge the support and contributions of the Audiology and Speech Center at the Walter Reed National Military Medical Center and the Department of Defense Hearing Center of Excellence in the development and extensive testing of this software.',
            title: 'Acknowledgement',
          },
        ]}
      </Block>
    );

    const Features = () => (
      <Block layout="fourColumn">
        {[
          {
            content: 'Develop custom hearing-related exams or general-purpose questionnaires, then deploy remotely to tablets and mobile devices at multiple sites.',
            title: 'TabSINT System',
            imageAlign: 'top',
            image: `${baseUrl}img/homepage.png`,
          },
          {
            content: 'Includes interface for plugins, allowing external contributors to build hardware that can interface directly with TabSINT.',
            title: 'External Hardware',
            imageAlign: 'top',
            image: `${baseUrl}img/wahts.png`,
          },
          {
            content: 'Housed on Gitlab, this software is available to researchers across the world to use and adapt to their specific needs. Researchers with engineering resources can also contribute to the repository to further improve the features and robustness of this software.',
            title: '[Open Source](https://gitlab.com/creare-com/tabsint)',
            imageAlign: 'top',
            image: `${baseUrl}img/gitlab.png`,
          },
        ]}
      </Block>
    );


    return (
      <div>
        <HomeSplash siteConfig={siteConfig} language={language} />
        <div className="mainContainer">
          <Features />
          <FeatureCallout />
          <HowToCite />
          <License />
          <Awknowledge />
        </div>
      </div>
    );
  }
}

module.exports = Index;
