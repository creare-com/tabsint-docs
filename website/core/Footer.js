/**
 * Copyright (c) 2017-present, Facebook, Inc.
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

const React = require('react');

class Footer extends React.Component {
  docUrl(doc, language) {
    const baseUrl = this.props.config.baseUrl;
    const docsUrl = this.props.config.docsUrl;
    const docsPart = `${docsUrl ? `${docsUrl}/` : ''}`;
    const langPart = `${language ? `${language}/` : ''}`;
    return `${baseUrl}${docsPart}${doc}`;
  }

  pageUrl(doc, language) {
    const baseUrl = this.props.config.baseUrl;
    return baseUrl + (language ? `${language}/` : '') + doc;
  }

  render() {
    return (
      <footer className="nav-footer" id="footer">
        <section className="sitemap">
          <a href={this.props.config.baseUrl} className="nav-home">
            {this.props.config.footerIcon && (
              <img
                src={this.props.config.baseUrl + this.props.config.footerIcon}
                alt={this.props.config.title}
                width="66"
                height="58"
              />
            )}
          </a>
          <div>
            <h5>Docs</h5>
            <a href={this.docUrl('introduction.html', this.props.language)}>
              Introduction
            </a>
            <a href={this.docUrl('quick-start/tabsint.html', this.props.language)}>
              Quick Start
            </a>
            <a href={this.docUrl('user-guide/background.html', this.props.language)}>
              User Guide
            </a>
          </div>
          <div>
            <h5>Source</h5>
            <a href="https://gitlab.com/creare-com/tabsint"
              target="_blank"
              rel="noreferrer noopener">
              GitLab
            </a>
            <a 
              href="https://gitlab.com/creare-com/tabsint/issues"
              target="_blank"
              rel="noreferrer noopener">
              Issue Tracker
            </a>
          </div>
          <div>
            <h5>Community</h5>
            <a
              href="https://www.youtube.com/channel/UCn0vindN1CIfxR3xKK4huaQ"
              target="_blank"
              rel="noreferrer noopener">
              YouTube
            </a>
          </div>
        </section>
        <section className="copyright">{this.props.config.copyright}</section>
      </footer>
    );
  }
}

module.exports = Footer;
