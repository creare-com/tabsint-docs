/**
 * Copyright (c) 2017-present, Facebook, Inc.
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

// See https://docusaurus.io/docs/site-config for all the possible
// site configuration options.


const siteConfig = {
  title: 'TabSINT', // Title for your website.
  tagline: ['Open source platform for hearing assessments', 'and general purpose questionnaires'],
  titleTagline: 'Creare',
  url: 'https://tabsint.org',
  baseUrl: '/', // Base URL for your project */

  // Used for publishing and more
  projectName: 'tabsint-docs',
  organizationName: 'creare',

  // Enable algolia search
  algolia: {
  	apiKey: '8c7e9f7c611970d91992d616f271766d',
  	indexName: 'tabsint'
  },

  // For no header links in the top nav bar -> headerLinks: [],
  headerLinks: [
    {doc: 'introduction', label: 'Docs'},
    {
      href: 'https://forum.tabsint.org',
      label: 'Forum',
    },
    {
      href: 'https://gitlab.com/creare-com/tabsint/-/releases',
      label: 'Releases',
    },
    {
      href: 'https://gitlab.com/creare-com/tabsint/blob/master/CHANGELOG.md',
      label: 'Changelog',
    },
    {page: 'about', label: 'About'},
    {search: true}
  ],

  /* path to images for header/footer */
  headerIcon: 'img/tabsint.svg',
  footerIcon: 'img/tabsint.svg',
  favicon: 'img/favicon.png',

  /* Colors for website */
  colors: {
    primaryColor: '#337ab7',
    secondaryColor: '#BBBCC7',
  },

  /* Custom fonts for website */
  
  fonts: {
    myFont: [
      "Times New Roman",
      "Serif"
    ],
    myOtherFont: [
      "-apple-system",
      "system-ui"
    ]
  },

  /* Other styling */
  scrollToTop: true,

  // This copyright info is used in /core/Footer.js and blog RSS/Atom feeds.
  copyright: `Copyright © ${new Date().getFullYear()} Creare`,

  highlight: {
    // Highlight.js theme to use for syntax highlighting in code blocks.
    theme: 'default',
  },

  // Add custom scripts here that would be placed in <script> tags.
  scripts: [
    'https://buttons.github.io/buttons.js',
    '/js/pdf417.js',
    '/js/qrcode.js',
    '/js/lz-string.js'
  ],

  // Allow static html pages to have separate styles
  separateCss: ['static/references'],

  stylesheets: ['static/css/custom.css'],

  // On page navigation for the current documentation page.
  onPageNav: 'separate',
  // No .html extensions for paths.
  cleanUrl: false,

  // Open Graph and Twitter card images.
  ogImage: 'img/logo.png',
  twitterImage: 'img/logo.png',

  // Show documentation's last update time.
  enableUpdateTime: true
};

module.exports = siteConfig;
