# TabSINT Docs

Documentation for [TabSINT](https://gitlab.com/creare-com/tabsint), an open source platform for administering tablet based hearing-related exams, as well as general-purpose questionnaires.

Built to http://tabsint.org on each commit.

See [website/README.md](website/README.md) for more information on documentation architecture and build.

## Install

Requires [NodeJS](https://nodejs.org/en/) 

From the root of the repository:

```bash
$ cd website
$ npm install
```

## Build

From the root of the repository:

```bash
$ cd website
$ npm run build
```

## Serve


From the root of the repository:

```bash
$ cd website
$ npm run start
```
