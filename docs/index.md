---
id: index
title: Introduction
---

If you are not redirected automatically, follow this <a href="../docs/introduction.html">link</a>.

<script type="text/javascript">
  window.location.href = '../docs/introduction.html';
</script>
