---
id: notices
title: Notices
sidebar_label: Notices
---

Please review the [Acknowledgment](https://gitlab.com/creare-com/tabsint/blob/master/README.md#acknowledgement), [LICENSE](https://gitlab.com/creare-com/tabsint/blob/master/LICENSE), and [NOTICE](https://gitlab.com/creare-com/tabsint/blob/master/NOTICE.md) documented in the source repository. 
