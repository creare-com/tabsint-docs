---
id: dosimeter
title: Dosimeter
sidebar_label: Dosimeter
---

The Svantek Dosimeter SV104A and Svantek Sound Level Meter SV973 have been integrated into TabSINT for recording background noise during TabSINT exams. TabSINT protocol pages can include a [`svantek`](https://gitlab.com/creare-com/tabsint/-/blob/master/src/res/protocol/schema/definitions/page.json#L478) boolean field to activate the feature, and the device can be connected/disconnected via controls on the [Setup Tab](configuration.html#setup-tab).

## Enable Bluetooth

Bluetooth must be enabled on the Svantek Devices in order to connect to TabSINT.  

To enable Bluetooth on the SV104A complete the following steps:

1.	Press and hold the bottom button to turn on.
2.	Press the bottom and left buttons at the same time.  Release and repeat.  You should see the Bluetooth option.
3.	Use the bottom button to switch Bluetooth to on.
4.	Press the right button to accept the change.
5.	Now you should see the Bluetooth symbol on the Svantek screen and you should be able to connect with TabSINT.

To enable Bluetooth on the SV973 complete the following steps:

1. Press ESC and Enter to open the Menu.
2. Select down arrow to highlight Instrument and hit Enter.
3. Select down arrow to highlight Comm. Ports and hit Enter.
4. Highlight Serial Port and use the left or right arrow buttons until Bluetooth is displayed. You will see the Bluetooth icon appear in the upper tool bar.
5. Select Enter and Esc to exit the Menu

## Connect to TabSINT

To connect the Svantek to TabSINT:

1. Make sure that Bluetooth is enabled on the Svantek.
2. From the [Setup Tab](configuration.html#setup-tab), connect to the dosimeter or the sound level meter via the `Connect` button in the `Dosimeter` section.
    - Note the active tasks will indicate the steps during connection, which may include powering on the device. Once done, the Admin page will display `State: 'connected'` in the `Dosimeter` section.

## Protocol Usage

To measure background noise during an exam using the Svantek Dosimeter or Sound Level Meter:

1. Add [`svantek: true`](https://gitlab.com/creare-com/tabsint/-/blob/master/src/res/protocol/schema/definitions/page.json#L478) to (a) page(s) in the `protocol.json` file.
    - Or use the [`cha-sandbox`](https://gitlab.com/creare-com/tabsint/-/blob/master/src/res/protocol/cha-sandbox/protocol.json) protocol for an example
2. (Optional) To see dosimeter background noise levels in the audiometry table at the end of an exam, add the `showSvantek` property to the protocol. See the [`cha-sandbox`](https://gitlab.com/creare-com/tabsint/-/blob/master/src/res/protocol/cha-sandbox/protocol.json) protocol for an example.
3. [Connect](#connect-to-tabsint) the Svantek to TabSINT.
4. Load the protocol containing pages with the `svantek` property.
5. Administer the exam. Each page with the property will automatically start recording when the page begins, and stop recording when it is finished
    - The results are published to the `page.results.svantek` property, which can be viewed during an exam when in admin mode by selecting the `show debug info` option, and navigating to the page results.

If the dosimeter is not connected when a test attempts to record from it, a notification will appear indicating such, then the test will run. Similarly, if connection is lost for any reason during an exam, the user will be notified and the test will continue running.

## Octave Band Measurement Option

The Svantek Dosimeter SV104A must be purchased with the octave band measurement option.  If it is purchased without this option, an activation code can be purchased after-the-fact and applied through the Supervisor software that comes on a disc with each Svantek device.

*Note that if you purchased the device with this option but received the device with a firmware version prior to 1.06.2, a firmware update to 1.06.2 (or later) may overwrite the octave band measurement option.  If so, you will need to contact Svantek to receive an activation code to be applied in the Supervisor software.*

To confirm that you have the option activated or apply the activation code:

1. Connect your Svantek to your computer via USB.
2. Run the Supervisor software.
3. Select your device type from the list of instruments on the left.
4. Right click on the connected dosimeter from the table at the top of the screen.
5. Select the `Manage options/functions` option.
6. If the option is enabled, it will list the state as `Enabled`.

<div type="text-align:center" height="400px"><img src="../assets/supervisor-activated.png"/></div>

7. If not, click the `Unlock` option in the `Manage instrument options/functions` window and enter the activation code when prompted.

<div type="text-align:center" height="400px"><img src="../assets/supervisor-activation.png"/></div>

## Svantek SV104A Control Settings

When TabSINT connects to the Svantek SV104A, it writes control settings to the dosimeter.  This is done by sending the command `#1,M3,f1;` to the serial port of the Svantek.  This command:

- `#1`: prepares to input the control setting codes
- `M3`: sets the measurement function to the 1/3 octave analyzer
- `f1`: sets the filter type to a Z-filter

You can reset these control settings through the Supervisor software.  To reset the settings:

1. Connect your Svantek to your computer via USB.
2. Run the Supervisor software.
3. Select your device type from the list of instruments on the left.
4. Right click on the connected dosimeter from the table at the top of the screen.
5. Select the `Send 'clear setup'` option.
6. Select `Yes` to continue.
7. Clearing the setup on the Svantek resets the device to the original settings, including disabling Bluetooth.  In order to be able to connect the Svantek to TabSINT again, you must follow the steps to [Enable Bluetooth](#enable-bluetooth).


<div type="text-align:center" height="400px"><img src="../assets/supervisor.png"/></div>


You can use the Supervisor software to confirm that the control settings have been correctly written to the Svantek:

1. Connect your Svantek to your computer via USB.
2. Run the Supervisor software.
3. Select your device type from the list of instruments on the left.
4. Highlight `Settings` in the `Instrument files` list and click the right-pointing arrow to copy the instrument settings to a local file.
5. Highlight `Settings` in the `Local files` list. The panel on the right will display the device settings.  After writing the control settings through TabSINT, you will see:
	- On the `Measurement` tab, the `Measurement Function` will be set to `Dosimeter and 1/3 Octave`.
	- On the `Time History` tab, the `Spectrum Logger` option will have a checkbox labeled `Leq` selected.
	- On the `Spectrum` tab, the `Filter` will be `Z`.

<div type="text-align:center" height="400px"><img src="../assets/supervisor-settings.png"/></div>
	