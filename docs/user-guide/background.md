---
id: background
title: Background
sidebar_label: Background
---

This section give a high-level overview of the TabSINT platform, server, and data structure.

## Software Overview ##

### Purpose ###

TabSINT is a flexible platform for administering hearing-related exams, as well as general-purpose questionnaires.
It is designed to be flexible, easy-to-use, and useful for administrators who manage studies of all sizes.  Exams and
questionnaires are presented as a series of pages on an off-the-shelf tablet using a crisp and intuitive interface.
Results are uploaded to a central server, through which administrators can design new exams and questionnaires,
as well as  download results.

### Architecture  ###

TabSINT follows a standard client-server architecture. There are two types of client devices: tablets (which administer exams) and standard administrator computers, which allow administrators to upload protocols, and to browse and download results.  The central server resides on Amazon Web Services and is backed up regularly.

<div type="text-align:center" height="400px"><img src="../assets/architecture.png"/></div>

### Technologies ###

TabSINT uses a variety of well-regarded tools and technologies. The exams are run on standard off-the-shelf Android tablets, specifically the Samsung Galaxy Tab E and Tab A. These tablets combine bright, high-resolution touchscreens with top-notch hardware, and are very reasonably priced. TabSINT supports both Wi-Fi-only and LTE-enabled tablets.

The server runs on a standard Ubuntu operating system on standard virtualized cloud computing hardware via
Amazon Web Services. The server is implemented in PHP and data are stored in a MySQL database.

The tablet client is implemented using standard web technologies: HTML5, CSS, and Javascript. Consequently, it can be ported relatively easily to different hardware systems, including iOS and Windows devices. It is packaged as a hybrid mobile app (that is, an app that is written with web technologies, but which functions as and appears as a native app) using the Cordova (aka PhoneGap) toolchain. The tablet interface makes extensive use of the Google-backed AngularJS platform, which greatly improves the interactivity of the software, and allows for extensive automated testing to help detect bugs.

<div type="text-align:center" height="400px"><img src="../assets/technologies.png"/><p>The TabSINT tablet application is built using the three primary web technologies: HTML5, CSS3, and Javascript.</p></div>

## Protocols ##

Protocols are critically important to configuring and using the TabSINT software. Protocols are the complete definitions of the text, media, logic, and configuration parameters that define a questionnaire or hearing exam. Furthermore, they are an important record in post-analysis, which (when combined with an exam results structure), document exactly how an exam was run.

Protocols are written in Javascript Object Notation (JSON), a simple human-readable text format. They contain:

- Images, audio files, and/or videos
- Audio calibration data
- Explanatory text and titles
- Logic which determines how and when the user sees various stimuli
- Numerous optional configuration parameters which customize the application's look, feel, and behavior

Please see [Developing Protocols](protocols#developing-protocols) for more details on how protocols are written and tested, and a formal definition of the information they can contain.

### Exam Pages ###

The bulk of a protocol is a series of **Exam pages**. As the exam progresses, TabSINT progresses from one exam page
to another.

Each page is composed of UI components, which are arranged in the same order on every page. An exam page might include
all, some, or (in the case of an empty page), none of these components. Taken together, these highly configurable
components can create a very diverse range of exam pages.

The following figure shows several of the basic components, all of which can be configured in the protocol file.

The most diverse and important component is the input response area. Each page can contain only a single input response. Each response area is specialized for a particular type of question. For example:
- a text box
- a multiple choice question
- a modified rhyme test (MRT)

and many others.

<div style="text-align:center"><img src="../assets/page_building_blocks.png"/></div>
