---
id: tablet-setup
title: Tablet Setup
sidebar_label: Tablet Setup
---

This section describes the process of setting up a new tablet. These basic instructions would apply to any tablet, Android or even Apple devices. However, instructions are modeled off the OS Android 8.1.0. If using a different OS, the order of procedures or location of needed files or applications may differ. 

## Tablet Models

The tablet models that have been tested and are actively used with TabSINT are:

- Samsung Galaxy Tab E (SM-T377A)
- Samsung Galaxy Tab A (SM-T380NZKEXAR)
- Samsung Galaxy Tab A7 Lite (SM-T220)
- Samsung Galaxy Tab A7 (SM-T500)*

*The Tab A7 has a larger screen of 10.4"


**For TabSINT v4.3.0 and all future releases we will only support Android operating system 7.0 and newer.**

## Tablet Start-up

The goal of this section is to run through Androids initial start up procedures and prepare the tablet for optimizaton with TabSINT and ease of use for the user.

The following has been customary for setting up a tablet for research or development purposes. Some modificaiton for one's personal use circumstance should not affect the tablets ability to interact effectively with TabSINT. In general it is recommended to follow the setup procedures as below and minimize background applications and other features that could interupt, disrupt or slow down the TabSINT application.

- **Welcome** When going through Androids start up procedure, choose defaults except where noted below. Do not accept any *extras* or *options* unless stated to do so.

- **Choose a Wi-Fi network** TabSINT requires a connection for downloading the initial application and for uploading results.
- **Copy apps & data** Select "Don't Copy"
- **Sign In** A google play store account is required to download TabSINT. Use an existing account and sign in here or set up a play store account later in tablet settings.
- **Google Services** Toggle OFF "Back up to Google Drive". Toggle OFF "use Location. (TabSINT required location permissions to work effectively with the WAHTS and permission will be granted after download). Toggle OFF "Allow Scanning". Toggle OFF "Send usage and diagnostic data"
- **Protect your tablet** User discretion 
- **Hey Google** Skip this 
- **Access Assistant** Skip this
- **Samsung Account** Skip. A play store account is required a samsung account is not.


## Adjust Settings

Adjust the settings to allow for a cleaner interaction with TabSINT by shutting down notifications, back ground apps and other possible interferences. Instructions below are specific to Android OS 11. The premise is the same regardless of the OS, but the names and locations of the settings may vary. If not explicitly mentioned use default settings or personal preference.

![Settings](../assets/Settings.png)

- **Select** the Settings icon and set the following settings:
	- *Connection*: Confirm Wi-Fi connection and Toggle ON bluetooth
    - *Sound*: Mute sound mode. **Select** "System Sound" and toggle OFF all sounds
	- *Notifications*: Toggle OFF all notifications. **Select** "Advanced settings" and Toggle OFF all notification excpet "Battery Percentage." Leave "Battery Percentage" Toggle ON. **Select** "Do Not Disturb" and Toggle ON.
    - *Display*: **Select** "Screen timeout" and set to highest value. 
    - *Location*: Toggle ON. Location permissions are required for the WAHTS to pair with TabSINT. 
    - *Google Account*: The google account set up for the google playstore can viewed here.
    - *Advanced features*: Toggle OFF all options
	- *Applications*: Turn off or disable as many as possible **except** Chrome, My Files, Settings, Tools, Gallery, Google Play Store and Google Play Services (these will be needed). Turning off and disabling application may also be done on the applications section described later
    - *Software Updates*: Toggle OFF Auto download over Wi-Fi. 
	- *About tablet*: Edit "Device Name" name device according to a serial identifier that works for your situation.

	

## Turn Off Samsung Updates

There are Samsung Apps which will automatically update and cause the apps you disable in the next step to re-appear.  To disable the automatic Samsung updates:

- **Tap** to open the Samsung folder.  Depending upon your specific software build you may see either an icon for *Galaxy Apps* or *Galaxy Store*.  If the *Galaxy Store* app appears you may proceed directly to that app
- **Tap** to open the *Galaxy Apps* and select *Update*
- **Tap** to open the *Galaxy Store*
- **Tap** the menu icon in the upper left or lower right and select the Settings icon
- **Select** *Never* under Auto Update Apps

## Turn Off Automatic App Updates

By default, Google Play will automatically update apps.  To disable the automatic updates:

- **Tap** to open the Google Play Store
- **Tap** to open the menu, either the three bars or profile icon
- **Tap** *Settings*
- **Tap** *General*  Select *Network preferences* and turn OFF Auto-updates

## Clean and Clear apps

New tablets arrive with many pre-installed apps. These are unnecessary for our application, clutter up the screen, slow down tablet, and are burdensome with notifications. Don't worry about cleaning up too much. Apps can always be re-installed or turned back on.

- **Keep** All apps may be uninstalled or disabled except *My Files*, *Settings* *Google play store*. These will be needed for TabSINT. Beneficial to keep based on preference *Internet Browser* of choice, *Gallery*, other tools  (e.g. calculator)

<div align="middle"><img src="../assets/Home-CleanUp.png" width="50%"/></div>

- **Tap and Hold** an icon on the home page until the *Remove* option appears, then **Tap** the *Remove* icon. Do this for **ALL** icons on the home screen.
- **Scroll** to view the remaining apps.
- **Uninstall or disable** the remaining unnecessary apps. Keep any for personal preference

<div align="middle"><img src="../assets/Apps-2.png" width="50%"/></div>

- **Arrange** applications on the page. There are some applications that Android will not allow to be deleted.  Stack those in a folder. The end result will appear similar to below.

<div align="middle"><img src="../assets/Apps-3.png" width="50%"/></div>

(Pick up HERE)

## Set Default Browser and Homepage

For convenience, set the default Internet browser to Chrome and the default home page to https://tabsint.org.
  
    
- Launch the **Chrome Browser**. 

![Chrome](../assets/chrome.png)

- Tap the **Menu icon** located in the upper-right.
    <div align="middle"><img src="../assets/menu.png" width="30%"/></div>
- Tap **Settings**
- From the *Basics section*, tap **Home Page**
- Tap the **Home Page switch** to on 
    <div align="middle"><img src="../assets/on.png" width="05%"/></div>
- Tap **Open this page** then enter the preferred URL for the Home page. In this case we recommend https://tabsint.org.
- Tap **Save**

## Download and Install TabSINT

To install and configure TabSINT, follow the procedure in the [TabSINT Quick Start Guide](http://tabsint.org/docs/quick-start/tabsint.html). You will need a Google Play store account to complete this step.

## For Developers

Developers or some advanced users may want to put the tablet in developer mode.  For these users, we recommend turning *USB debugging* on. 

- Go to **Settings** and scroll down to **About tablet**
- Tap **Software information**
- Tap **Build Number** 7 times. A notice that Developer Mode is being activated will appear
- In **Settings** select **Developer Options** and toggle **USB debugging** to on

For more on Developing with TabSINT visit the [Developer Guide](https://gitlab.com/creare-com/tabsint/-/blob/develop/developer_guide/developer.md) section of the TabSINT source code.




