---
id: protocols
title: Protocols
sidebar_label: Protocols
---

TabSINT supports many features for developing test protocols out of the box.  Test protocols can be developed to accommodate for a wide range of research studies.  Protocols allow you to customize your exam in TabSINT.  See the [TabSINT Protocol Workshop](../references/references#tabsint-protocols) references for more information.

## What is a protocol?

A protocol is a machine-readable file that defines the content and logic for a set of interactive pages. These pages may be organized to form unique and complex test protocols and questionnaires.

Protocol pages can contain text, audio, images, and videos, as well as 25+ predefined [response areas](response-areas) and [WAHTS-specific response areas](wahts-response-areas).  These response areas allow for the input of text and integer data, as well as the execution of audiometry exams and many standard speech in noise tests.

### Protocol Files

At a minimum, a protocol should be a folder with a meaningful name and **must** contain a file named `protocol.json`.  The `protocol.json` file may also reference files:

- `.wav`, `.mp3`: audio files
- `.png`, `.jpg`: image files
- `.mp4`, `.gif`: video files
  
Supporting files may be stored in subdirectories, but `protocol.json` **must** be at the top level. 
All file references must be relative to the root protocol directory (i.e. `images/image1.png`).

### Format ###

Protocols are written in the text-based [JavaScript Object Notation (JSON)](http://www.json.org) format.  This format is commonly used for transmitting data objects that are both human and machine readable.

```
{
	"key": "value",
	"page": {
		"name": "json example"
	}
}
```

Because this format is machine readable, the syntax is very strict.  Each comma, brace, and quote is significant.

The basic JSON structure is:

- double quotes around keys and values, which are paired with a colon (`"key": "value"`)
- curly braces to group key/value pairs (`{ "key": "value" }`)
- square braces to denote an array of similar items (`[1, 2, 3]`)
- commas to separate groups (`[{ "key": "value" }, { "key": "value" }]`)
- line breaks are insignificant (they are an aid for the reader but have no syntactic meaning)

> **Warning**: The JSON structure makes it relatively easy for a human to understand the content of the document at a glance, but writing a JSON document can be tricky since every double quote, colon, comma, curly brace, and square brace has syntactic significance.  Misplacing even a single one will result in a syntax error.

Attention to detail, along with frequent [syntax validation](https://jsonlint.com), will ease the development of the protocol.

### Protocol Schema ###

In addition to writing a syntactically valid JSON document, it must be structured in accord with a **schema**.
The schema defines the properties and property types allowed to be used in a protocol.  All protocols must adhere to the schema to ensure that TabSINT will know how to interpret the protocol file.

The complete [TabSINT Protocol Schema](https://gitlab.com/creare-com/tabsint/tree/master/src/res/protocol/schema) is hosted in the [TabSINT source code](https://gitlab.com/creare-com/tabsint/) repository on Gitlab.

For more information on how JSON schema files are structured see [http://json-schema.org/](http://json-schema.org/).

### Elements of a Protocol ###

- **Global Properties**: Protocols contain a set of top level properties that define the parameters and context in which a protocol should be administered. The global properties are inherited by each page and can be used to customize the protocol flow.  Global properties are defined in [protocol_schema.json](https://gitlab.com/creare-com/tabsint/-/blob/master/src/res/protocol/schema/protocol_schema.json).
- **Pages**: Protocols contain a set of pages that are presented to the user in a specified order. Each page contains a combination of text and media to present to the user and a response-area to collect a user response. Page properties are defined in [page.json](https://gitlab.com/creare-com/tabsint/-/blob/master/src/res/protocol/schema/definitions/page.json).
- [**Response Areas**](response-areas): The user response can be obtained in many formats, including yes/no, integer, multiple choice, checkbox, Likert, MRT, OMT, and QR codes.
- [**Subprotocols**](#subprotocols): A protocol can have different sections, referred to as ``subProtocols``, which have their own global properties (set according to [protocol_schema.json](https://gitlab.com/creare-com/tabsint/-/blob/master/src/res/protocol/schema/protocol_schema.json)), including page defaults and page randomization.
- [**Dynamic Content**](advanced-protocols#dynamic-logic): In addition to these static attributes, a protocol can be made dynamic using customized logic between pages. This logic includes the capability to skip pages, set test flags, and ask follow on questions. 
  

#### Simple Protocol Example ####

```
{
	"title":"A Very Simple Test",
	"pages":[
		{
			"id":"question1",
			"questionMainText":"How many years of service do you have?",
			"responseArea": {
				"type":"integerResponseArea"
			}
		},
		{
			"id":"question2",
			"questionMainText":"How many times have you been deployed to Iraq or Afghanistan?",
			"responseArea":{
				"type":"integerResponseArea"
			}
		}
	]
}
```

This protocol presents 2 pages, both of which contain question text and an integer response.
The global ``title`` property is inherited by each of the pages, but each page defines its own ``questionMainText`` to present.

### Subprotocols ###

Sets of pages can be grouped together into larger sections referred to as `subProtocols`.
`subProtocols` can define specific attributes to a subset of pages within a larger protocol.

For example, one `subProtocol` may specify a certain type of randomization, while another does not.  They can also be used to repeat or reuse groups of pages in different parts of the protocol.

#### Example Using Subprotocols ####

```
{
	"title":"A Simple Multiprotocol Exam",
	"pages":[
		{
    			"reference": "no_randomization"
    		},
    		{
    			"reference": "randomized",
    		}
    	],
    	"subProtocols":[
    		{
    			"protocolId": "no_randomization",
    			"pages":[
    				{
    					"id":"question 1-1",
    					"questionMainText":"How many years of service do you have?",
    					"responseArea":{
    						"type":"integerResponseArea"
    					}
    				},
    				{
   	 				"id":"question 1-2",
    					"questionMainText":"How many times have you been deployed to Iraq or Afghanistan?",
    					"responseArea":{
    						"type":"integerResponseArea"
    					}
    				}
    			]
    		},
    		{
    			"protocolId": "randomized",
    			"randomization": "WithoutReplacement",
    			"pages":[
    				{
    					"id": "question 2-1",
    					"questionMainText": "What is your age?",
    					"responseArea":{
    						"type": "integerResponseArea"
    					}
    				},
    				{
    					"id": "question 2-2",
    					"questionMainText": "What is your favorite number?",
    					"responseArea": {
    						"type": "integerResponseArea"
    					}
    				}
    			]
    		}
    	]
}
```

## Distributed Protocols

There are full protocols [distributed](../quick-start/run-exam#distributed-protocols) with this software which are good examples to work from when developing a new questionnaire. The protocols distributed with TabSINT are in the [src/res/protocol](https://gitlab.com/creare-com/tabsint/-/tree/master/src/res/protocol) folder of the source code and are available in the [Protocols](configuration#protocols) table in TabSINT.

[Audiometry](https://gitlab.com/creare-com/tabsint/-/blob/master/src/res/protocol/edare-audiometry/protocol.json)  

[Creare Audiometry](https://gitlab.com/creare-com/tabsint/-/blob/master/src/res/protocol/creare-audiometry/protocol.json)

[tabsint-test](https://gitlab.com/creare-com/tabsint/-/blob/master/src/res/protocol/TabSINT-Test/protocol.json) (contains examples of all of the non-WAHTS [Response Areas](response-areas))

[wahts-software-test](https://gitlab.com/creare-com/tabsint/-/blob/master/src/res/protocol/wahts-software-test/protocol.json) (contains examples of all of the [WAHTS Response Areas](wahts-response-areas))


## Developing Protocols

The following section will provide an overview on the methods for developing protocols.

More advanced tools for developing protocols are described in the [Protocol Development Tools](https://gitlab.com/creare-com/tabsint/-/tree/develop/developer_guide/protocol-dev.md) section  of the [Developer Guide](https://gitlab.com/creare-com/tabsint/-/tree/develop/developer_guide/developer.md).

### Tools for Writing JSON ###

There are many tools that can assist in writing syntactically valid protocols.  

**Text Editors**

The following text editors auto indent and highlight matching braces:

- [Notepad++](https://notepad-plus-plus.org)
- [Notepad2](http://www.flos-freeware.ch)
  
**Online Editors**

- [JSON Editor Online](http://www.jsoneditoronline.org): An online editor that presents both a text view and object view. The object view presents a hierarchical representation of the JSON document.

**Syntax Validators**

- [JSON Lint](https://jsonlint.com): Copy and paste the protocol text into this editor and it will confirm that JSON syntax is valid.

### Development Environment

In order to develop protocols most effectively, a user should have:

- A [text editor](#tools-for-writing-json-documents)

- A recent distribution of the [TabSINT source code](https://gitlab.com/creare-com/tabsint)

- A tablet with the [TabSINT release](https://play.google.com/store/apps/details?id=com.creare.skhr.tabsint&hl=en_US) installed
      
### Development Workflow

When developing protocols, you can iterate and test on a tablet or in a web browser.  While the tablet method is appropriate for most users, more advanced protocol developers may choose to use the web browser method (documented in the [Protocol Development Tools](https://gitlab.com/creare-com/tabsint/-/tree/develop/developer_guide/protocol-dev.md) section  of the [Developer Guide](https://gitlab.com/creare-com/tabsint/-/tree/develop/developer_guide/developer.md)).

To test a protocol on your tablet:


1. Draft your protocol.
2. [Validate](https://jsonlint.com) the syntax of your protocol.
3. Check the `Validate protocols` option on the [Protocols Tab](configuration.html#protocols-tab) (to confirm that your protocol validates against the TabSINT protocol schema)
4. Load the protocol on your tablet via [Device Storage](data-interface#device-storage) or a [Gitlab](data-interface#gitlab) repository.
    - If the protocol fails validation, edit the protocol so that it adheres to the TabSINT schema.  Then go back to Step 2.
5. Run through the exam.
6. Make any desired changes in the `protocol.json` file.
7. Iterate steps 2-5 until the desired functionality is reached.

