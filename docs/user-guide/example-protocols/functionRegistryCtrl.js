(function() {

  tabsint.register('changeText', function(dm) {
    var response = dm.result.response;
    var newQuestionMainText;
    var newChoices;

    if (response === 'A'){
      newQuestionMainText = 'Do you like baseball?';
      newChoices = [{id:'A',text:'Yes I like Baseball'},{id:'B',text:'No, I do not like Baseball'}]
    } else if (response === 'B'){
      newQuestionMainText = 'Do you like cars?';
      newChoices = [{id:'A',text:'Yes I like Mustangs'},{id:'B',text:'No, I ride my bike'}]
    }

    return {
      questionMainText: newQuestionMainText,
      responseArea: {
        choices: newChoices}
    };
  });

  tabsint.register('addFollowOn', function(dm) {
    // add a followOn and conditional flag

    var newSetFlags = [
      {
        id:'DO_FOLLOW_ON',
        conditional:"result.response === 'y'"
      }
    ];

    var newFollowOns = [
      {
        conditional:'flags.DO_FOLLOW_ON',
        target:{
          id:'ynFollowOn',
          questionMainText:'Are you enjoying this follow-on?',
          wavfiles:[
            {
              path:'chirpFullScaleWRTRef.wav',
              targetSPL:'80'
            }
          ],
          responseArea:{
            type:'yesNoResponseArea'
          }
        }
      }
    ];

    return {
      followOns: newFollowOns,
      setFlags: newSetFlags
    };
  });

})();



