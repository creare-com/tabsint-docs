---
id: configuration
title: Configuring TabSINT
sidebar_label: Configuration
---

This page gives an overview of how to start, setup, and use the TabSINT software.

## Starting TabSINT ##

TabSINT opens to the launch screen, showing the options:

<div style="text-align:center"><img src="../assets/welcome.png" width="50%"/></div>

- **Scan Configuration**: This option is for scanning a previously generated [QR code](qr-generate) for configuring TabSINT. 
- **Exam View**: This option will open the exam view for the current active protocol. If no protocol has been loaded, the page will display *Exam Disabled*.
- **Admin View**: This option will open the administrator console and may require a pin to access. The admin console provides the ability to load protocols, view results, and configure the application.
- **Documentation**: This option will open this documentation in a local browser window on the tablet.

## Admin View ##

To access the configuration options in TabSINT:

- From the launch screen (or the menu in the top-right), select **Admin View**
- If prompted, type in the **Admin PIN** and click *OK*
- You are now in the Admin view, with access to [Setup Tab](#setup-tab), [Protocols Tab](#protocols-tab), [Results Tab](#results-tab) and the [Exam View](#administering-an-exam).

### Setup Tab ###

#### TabSINT ####

To view all of the TabSINT configuration options, select **Show Advanced Settings** in the TabSINT section.

<div style="text-align:center"><img src="../assets/setUpTablet.png" width="100%"/></div>

- ``Default Headset``: Ensure that this matches the type of headset that you have connected to the tablet (if any). If this value is set incorrectly, then the tablet audio may **not** be calibrated properly.
- ``Admin Mode``: When Admin Mode is enabled, additional configuration options are available, debug information is available at the bottom of exam pages, and prompts to enter the **Admin Pin** are suppressed. Leave this option *unchecked* unless you are developing protocols or making configuration changes. 
- ``Admin PIN``: Click the pencil icon to change the admin pin.
- ``Disable Logs``: When checked, automatic software logging in the TabSINT software is disabled (see [logging](../introduction.html#logging) for privacy details).
- ``Max Log Length``: This sets the maximum number of log records to be saved to prevent consumption of too much tablet memory. Click the pencil icon to change.
- ``Disable Automatic Volume Control``: This option will disable TabSINT from setting the volume to 100% on every page. Check this box if you would like to set the volume of the app manually using the volume buttons on the outside of the device.
- ``Enable Skip in Exams``: Check to enable skipping on every page of an exam.
- ``Require Encryption``: Check to encrypt test results as described in the [Encryption Strategy](analysis#data-encryption).
- ``Record Test Location``: Check to enable location tracking.
- ``QR Code``: Click *Generate* to save the current configuration into a scannable [QR Code](qr-generate).  This QR code can then be scanned to recreate the current configuration on this or any other tablet.
- ``Reset configuration``: Click *Reset* to reset the configuration settings to the default configuration.
- ``Automatically output test results``: When checked, automatically output test results in the *Results Mode* selected as soon as the exam is completed.
- ``Results Mode``: Set whether the results can be uploaded only, exported only, or uploaded/exported.
- ``Local Results Directory``: Set the local directory for saving results
- ``Tablet Gain``: Automatically set to the correct value for the tablet.

#### WAHTS ####

To view all of the WAHTS configuration options, select **Show Advanced Settings** in the WAHTS section.

<div style="text-align:center"><img src="../assets/setUpWAHTS.png" width="50%"/></div>

- ``Type``: Type of connection to the WAHTS.  Should be `Bluetooth 3.0` except when transferring [media files](cha#loading-audio-files) to the headset.
- ``Disable Audio Streaming``: When checked, disable the streaming feature.
- ``Ignore Firmware Update Message``: When checked, allow headset connection to continue without firmware updates.
- ``Auto Shutdown Time``: This sets the amount of time in minutes a WAHTS headset will stay active before shutting down. Click the pencil icon to change.
- ``Enable Headset Media Management``: When checked, enable the transfer of [media files](cha#loading-audio-files) to the headset.

#### Dosimeter ####

Connect to a [Svantek dosimeter](dosimeter), if using.

#### Flic Buttons ####

Connect to a Flic Bluetooth button, if using.

#### Software and Hardware ####

This section displays all of details regarding the TabSINT software version and the tablet hardware information.

<div style="text-align:center"><img src="../assets/setUpSoftware.png" width="50%"/></div>

#### Application Log ####

This section allows you to display and upload/export the application logs (if logging is enabled in [TabSINT](#tabsint)).

<div style="text-align:center"><img src="../assets/setUpLog.png" width="50%"/></div>

### Protocols Tab ###

#### Protocols ####

The Protocols view lists the protocols that are loaded into TabSINT and ready to use. TabSINT can hold many protocols locally, but only one protocol is active at a time. Each protocol available is listed by name, version, and date.  The built-in protocols that are always available with the TabSINT software will be listed with the version as the TabSINT software version.  The currently active protocol (if any) will be shaded yellow.

<div style="text-align:center"><img src="../assets/protocol-view2.png" width="50%"/></div>

Protocols are managed by selecting the protocol in the table and then selecting one of the options:

- ``Load``: Load, validate, and activate the selected protocol in the Exam View
- ``Update``: Check for new versions of the protocol (for TabSINT Server and Gitlab protocols only)
- ``Delete``: Remove the protocol from the locally available protocols (not available for the built-in protocols)
  
  - The protocol information will be removed, and the protocol files will be deleted (if the protocol was loaded via Device Storage, the protocol will still be available in the tablet files)

<div style="text-align:center"><img src="../assets/protocol-table-options.png" width="50%"/></div>

#### Source ####

Select `TabSINT Server`, `Gitlab` or `Device Storage` to set the location from which to download new protocols and where to upload results.  For more information on the options for sourcing protocols, see [Data Interface](data-interface).

- `TabSINT Server`: Use a TabSINT specific server to download protocols and export results based on site names
- `Gitlab`: Use your own Gitlab repository to download protocols, export results and for using a common media repository
- `Device Storage`: Use the tablet file system to load protocols and export results

##### TabSINT Server #####

>[TabSINT Server](data-interface#tabsint-server) is currently closed source and only available to some users

<div style="text-align:center"><img src="../assets/tabsint-server-config.png" width="50%"/></div>

- ``URL``: The web address to your TabSINT server
- ``Username`` and ``Password``: Your login credentials
- ``Valid``: This will validate your login credentials against the input URL

If the protocol exam was added to TabSINT from a specific TabSINT server, results will be uploaded to this same server. Details for accessing this server would be provided by the study coordinator. 


##### Gitlab #####

<div style="text-align:center"><img src="../assets/gitlab-server-config.png" width="50%"/></div>

- ``Repository``: Name of the protocol repository on Gitlab
- ``Version``: Specify version to load (loads latest version if not specified)
- ``Host``: The web address to your gitlab account (i.e. ``https://gitlab.com/``)
- ``Token``: The personal access token to your gitlab account
- ``Group``: The group name of your gitlab repository (can include subgroups)
- ``Only Track Tags``: If checked, track by tags (otherwise track by commits)
- ``Change Results Location``: Designate location for results to upload

See [Gitlab](data-interface#gitlab) documentation for more information

##### Device Storage #####

<div style="text-align:center"><img src="../assets/deviceStorageConfig.png" width="100%"/></div>

- ``Directory``: Select `+ Add`, then navigate from ``Documents/tabsint-protocols`` on the internal storage to the directory with your `protocol.json` file.  Then press `Use This Folder` in the bottom of the pop-up to add the protocol. You must select "Allow" when prompted by the tablet to allow TabSINT access to the folder. 

See [Device Storage](data-interface#device-storage) for more information

### Results Tab ###

#### Completed Tests ####

This view lists all the test results currently stored in TabSINT's memory. Results are displayed here until they are uploaded to a configured server or deleted by an administrator.  Each result will list the protocol name, test start time and test duration.  The button options will depend on `Results Mode` from the [Setup Tab](#tabsint).  Selecting these button options will apply to ALL results listed here.

<div style="text-align:center"><img src="../assets/results2.png" width="50%"/></div>

Select a specific result to view the details of that result.  From there you can:

  - Tap any field to expand.  For example, to see responses to specific questions, tap ``testResults``, then ``responses``.
  - ``Upload``: Upload this specific result now (Gitlab and TabSINT servers)
  - ``Export``: Export this result to a local file on the tablet
  - ``Delete``: Delete this result
  
<div style="text-align:center"><img src="../assets/results-detail.png" width="50%"/></div>

Uploading sends exam results to either the Gitlab repository shared with the location of the exam protocol or to the TabSINT Server shared with the exam protocol and can only be done for protocols loaded via Gitlab or the TabSINT Server.

Any result can be exported onto the tablet device storage regardless of how the protocol was loaded as long as the `Results Mode` from the [Setup Tab](#tabsint) is set to either `Upload/Export` or `Export Only`.  The default local results directory is *tabsint-results*.  To save to a different location, see the [Setup Tab](#tabsint).

#### Recently Exported ####

Once a result has been uploaded or exported, it is moved from the Completed Tests list to the Recently Exported list.  These files can no longer be viewed from the tablet.

### Administering an Exam ###

While administering an exam will depend on the exact protocol in use, the steps to begin and end an exam are the same:

1. [Load a protocol](#protocols) in the [Protocols Tab](#protocols-tab) of the Admin View.
2. Navigate to the Exam View tab.
3. Be sure that the subject is ready to start the exam, and communicate any necessary oral instructions.
4. Press `Begin` and hand the tablet to the subject.
5. Administer the protocol. When the protocol finishes, the subject should return the tablet to the administrator.
6. Once the exam reaches the final screen, the results will automatically be queued for export or upload. The exam results will also get backed up in a local file on the tablet. 
	- At this point, the exam results are viewable in the [Completed Tests](#completed-tests) portion of the [Results Tab](#results-tab).
	- If TabSINT is configured to upload results to a remote server and `Automatically output test results` is selected (see the [TabSINT](#tabsint) portion of the [Setup Tab](#setup-tab)), the exam result will attempt to upload at this point.  If the result is successfully uploaded, it will be moved to the [Recently Exported](#recently-exported) portion of the [Results Tab](#results-tab). 
7. Press the ``New Exam`` button if you would like to run another exam on the same protocol.  
