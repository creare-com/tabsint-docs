---
id: cha
title: WAHTS
sidebar_label: WAHTS
---

The WAHTS is a hearing assessment device that can be used with TabSINT.

## Basics

To run a protocol with [WAHTS exam pages](wahts-response-areas):

1.  Make sure the WAHTS is powered on
2.  Navigate to the [WAHTS](configuration.html#wahts) section in the Setup Tab of the Admin View
3.  Select the appropriate Bluetooth type (should be `Bluetooth 3.0` to run an exam)
4.  Select `Connect`
5.  Choose the appropriate WAHTS from the list of available devices
6.  When the connection is complete, the WAHTS info will be displayed

## Streaming Audio to the WAHTS

The WAHTS can be used as a standard Bluetooth headset to play any audio from the tablet.  To use the WAHTS as a Bluetooth headset:
- select `WAHTS` from the `Default Headset` drop-down menu in the [TabSINT Configuration](configuration.html#tabsint)
- uncheck `Disable Audio Streaming` in the [WAHTS Configuration](configuration.html#wahts)

Now the audio from any media will play through the WAHTS.

## Loading and Playing Audio Files on the WAHTS

TabSINT can play audio files that are stored on the WAHTS.  These audio files may be pre-loaded on the WAHTS file system, or they may be audio files loaded onto the WAHTS by the user.

Files loaded by the user onto the WAHTS will be placed in the `USER` directory.  When referencing a file in a TabSINT protocol, the path must be prefixed by `C:USER`.

### Loading Audio Files

#### Using CHAMI

To load audio files onto the WAHTS using CHAMI, see the `write_file` method in the [CHAMI User Manual](/references/CHAMI-User-Manual.pdf).  An example script that includes rescaling and resampling wavfiles (`write_media.m`) is provided in the [TabSINT MATLAB Tools](https://gitlab.com/creare-com/tabsint/tree/master/tools/matlab).

#### Using TabSINT

TabSINT can also load audio files onto the WAHTS from any Gitlab repository.  To load audio files onto the WAHTS via Gitlab:

* Create a repository in your Gitlab group with the desired audio files
* Tag the repository at the desired commit
* Download the repository to your tablet
  * In the protocol [Source](configuration.html#source) configuration, enter your repository's `Host`, `Group` and your access `Token` 
  * In the [TabSINT](configuration.html#tabsint) configuration, confirm that `Admin Mode` is checked
  * In the [WAHTS](configuration.html#wahts) configuration, check `Enable Headset Media Management` 
  * Enter your media repository name and tag version (if desired)
  * Select `+ Add`
  * This will likely take > 5 minutes (depending on the number of files)
* Sync the media to your WAHTS
  * Make sure that your WAHTS and tablet are fully charged
  * Connect the WAHTS to the tablet using a Y-shaped USB OTG (On-The-Go) cable (like [this one](https://www.amazon.com/AuviPal-Micro-USB-Cable-Power/dp/B07FY9Z9GD/ref=sr_1_1?dchild=1&keywords=USB+OTG+Y-cable&qid=1616100951&sr=8-1), for example.  A standard micro-to-micro cable will not work, it must be OTG)
  * In the [WAHTS](configuration.html#wahts) configuration, for `Type` select `USB Host` from the drop-down menu
  * Tap `Connect`
  * You may get a pop-up `Allow the app TabSINT to access the USB device?`.  Select OK. 
    * Depending on how long it takes you to select `OK`, the connection attempt may fail with a pop-up telling you TabSINT could not connect to the headset.  Just tap `Connect` again.
  * Select the media repository you added, then select `Sync to Headset`.  
    * NOTE: do NOT disconnect the WAHTS from the tablet during sync.  This action can lead to corrupted files, and necessitate reformatting the WAHTS SD card and replacing all the media!
    * First TabSINT will compare all media files in the repository with the files on the WAHTS.  This can take several minutes for large repositories with a lot of files.
    * TabSINT will then delete any directories or files found on the WAHTS that are no longer in the repository.
    * Next, TabSINT will transfer all new files to the WAHTS.  This step provides progress updates based on number of files and number of kBytes to transfer.  It will take awhile.

  <div style="text-align:center"><img src="../assets/media-transfer-connection.png" width="60%"/></div>

### Playing Audio Files on the WAHTS

Any exam page can play audio files that are stored on the WAHTS.  Playing wav files on the WAHTS is very similar to playing wav files on the tablet, except instead of using the `wavfiles` parameter, you would now use the `chaWavFiles` parameter.  See the [`chaWavFiles`](https://gitlab.com/creare-com/tabsint/-/blob/master/src/res/protocol/schema/definitions/page.json#L159) parameter within the `page` field of the protocol schema for the precise syntax.

Below are two examples showing how to play a wav file on the WAHTS. The first example shows how to reference a wav file that is preloaded on the WAHTS, and the second shows how to reference a wav file that is loaded by the user onto the WAHTS:

```json
{
  "id":"wahtsWavFileExample",
  "title":"ChaWavs",
  "questionMainText": "This page plays a wav file stored on the CHA.  What did you hear?",
  "chaWavFiles":[
    {
      "path": "C:HINT/LIST1/TIS001.WAV"
    }
  ],
  "responseArea":{
    "type":"multipleChoiceResponseArea",
    "choices":[
      {
        "id":"Choice 1"
      },
      {
        "id":"Choice 2"
      }
    ]
  }
}
```

```json
{
  "id":"chaWavFileExample",
  "title":"ChaWavs",
  "questionMainText": "This page plays a wav file stored on the CHA.  What did you hear?",
  "chaWavFiles":[
    {
      "path": "C:USER/dir/myfile.wav"
    }
  ]
}
```

# Audiometry

TabSINT and The CHA peripheral offer Hughson-Westlake audiometry exams with a number of options:

* Automated vs Manual
* Threshold vs Screener

## Automated Screener

The Automated Screener exam repeats pulse trains at the same frequency and level a set number of times, and keeps track of how many
the listener hears.  The result is Pass/Fail, based on whether the listener heard a set number of pulse trains or not.

The implementation uses the HughsonWestlake responseArea, with the optional *Screener* field.

### Example: Automated Audiometry Screener

```json
{
  "id": "left_ear",
  "title": "Audiometry Screener Demo",
  "questionMainText": "Level: 45dB SPL, Ear: Left",
  "helpText": "Follow instructions",
  "instructionText": "Tap the button once for each set of sounds you hear",
  "responseArea": {
    "type": "chaHughsonWestlake",
    "examInstructions" : "Tap the button once for each sound you hear.",
    "examProperties": {
      "LevelUnits": "dB SPL",
      "Lstart": 45,
      "F":1000,
      "TonePulseNumber":3,
      "OutputChannel": "HPL0",
      "UseSoftwareButton": true,
      "PollingOffset": 1000,
      "MinISI":1000,
      "MaxISI":3000,
      "Screener": true,
      "PresentationMax": 3,
      "NumCorrectReq": 2
    }
  }
}
```

## Automated Threshold

The Automated Threshold exam repeats pulse trains at the same frequency but with dynamic level, in search of the listener's
threshold hearing level at the set frequency.  The result is a hearing threshold and the level progression the algorithm
followed to converge on the returned threshold.

### Example: Automated Audiometry Threshold

```json
{
  "id": "left_ear",
  "title": "Audiometry Hughson-Westlake Demo",
  "questionMainText": "Starting Level: 45dB SPL, Ear: Left",
  "helpText": "Follow instructions",
  "instructionText": "Tap the button once for each set of sounds you hear",
  "responseArea": {
    "type": "chaHughsonWestlake",
    "examInstructions" : "Tap the button once for each sound you hear.",
    "examProperties": {
      "LevelUnits": "dB SPL",
      "Lstart": 45,
      "F":1000,
      "TonePulseNumber":3,
      "OutputChannel": "HPL0",
      "UseSoftwareButton": true,
      "PollingOffset": 1000,
      "MinISI":1000,
      "MaxISI":3000
    }
  }
}
```

## Manual Audiometry

For more flexibility and control over the exam, audiometry exams can also be run manually.  In manual audiometry
response areas, the test administrator can select frequency, level, and ear for each sound presentation.  The administrator
also indicates when a threshold has been found for each frequency/ear combination.  Any combination can be repeated or
skipped.

### Example: Manual Audiometry Threshold

```json
{
  "id": "manualaudiometry",
  "title": "Manual Audiometry",
  "questionMainText": "Manual Audiometry",
  "helpText": "Follow instructions",
  "responseArea": {
    "type": "chaManualAudiometry",
    "presentationList": [
      { "F": 1000 },
      { "F": 2000 },
      { "F": 3000 },
      { "F": 4000 },
      { "F": 6000 },
      { "F": 1000 },
      { "F": 500 },
      { "F": 250 }
    ],
    "examProperties": {
      "LevelUnits": "dB SPL",
      "TonePulseNumber": 5,
      "Lstart": 50,
      "OutputChannel": "HPR0",
      "UseSoftwareButton": true,
      "PollingOffset": 600
    }
  }
}
```

For the screener version, the test administrator indicates pass/fail instead of indicating a threshold has been found.

### Example: Manual Audiometry Screener

```json 
{
  "id": "ManualScreener",
  "title": "Manual Screener",
  "questionMainText": "Manual Screener",
  "helpText": "Follow instructions",
  "responseArea": {
    "type": "chaManualAudiometry",
    "minLevel": 0,
    "maxLevel": 50,
    "responseType": "pass-fail",
    "presentationList": [
      { "F": 1000 },
      { "F": 500 },
      { "F": 1000 },
      { "F": 2000 },
      { "F": 3000 },
      { "F": 4000 },
      { "F": 6000 },
      { "F": 8000 }
    ],
    "examProperties": {
      "LevelUnits": "dB HL",
      "Lstart": 25,
      "TonePulseNumber": 5,
      "OutputChannel": "HPL0",
      "UseSoftwareButton": true,
    }
  }
}
```

## Run a list of Audiometry Exams

To run several audiometry exams with similar properties, use the *chaAudiometryList* responseArea.

* Set page properties for all exams, such as *autoSubmit* or *repeatOnce*, in the *commonResponseAreaProperties* field.
* Set exam properties for all exams, such as *F* or *Lstart*, in the *commonExamProperties* field. These properties will
be applied to every exam.
* Set individual exam properties for each exam in *presentationList*.  These exam properties will override *commonExamProperties*.

```json 
{
  "id": "AudiometryList",
  "title": "Left Ear List",
  "questionMainText": "Hughson-Westlake",
  "helpText": "Follow instructions",
  "instructionText": "This test measures your hearing sensitivity.  You will hear sounds at different pitches one ear at a time.  Your task is to tap the button when you hear a sound, no matter how soft the sound may be.",
  "responseArea": {
    "type": "chaAudiometryList",
    "presentationList": [
      {"F": 500},
      {"F": 1000},
      {"F": 2000, "Lstart": 50}
    ],
    "commonExamProperties": {
      "Lstart": 40,
      "UseSoftwareButton": true,
      "LevelUnits": "dB SPL",
      "OutputChannel": "HPL0"
    },
    "commonResponseAreaProperties": {
      "autoBegin": true,
      "autoSubmit": true,
      "repeatOnce": true
    }
  }
}
```

## Repeating Individual Audiometry Exams

Individual Audiometry exams have the ability to automatically repeat an exam if the exam does not successfully find a
threshold.

* *repeatIfFailedOnce* - Display a message encouraging the listener to listen carefully and then repeat a failed exam.
This option will present a repeat of the failed exam, using all the same examProperties.  The failed exam results will
be overwritten with the results of the repeat.
* *getNotesIfFailedTwice* - Collect notes from the test administrator upon a second failure.  This option
will display a message asking the listener to hand the tablet to the study administrator and provide a notes text box.

```json
{
  "id": "left_ear",
  "title": "Audiometry Hughson-Westlake Demo",
  "questionMainText": "Starting Level: 45dB SPL, Ear: Left",
  "helpText": "Follow instructions",
  "instructionText": "Tap the button once for each set of sounds you hear",
  "responseArea": {
    "type": "chaHughsonWestlake",
    "examInstructions" : "Tap the button once for each sound you hear.",
    "repeatIfFailedOnce": true,
    "getNotesIfFailedTwice": true,
    "examProperties": {
      "F":1000,
      ...
    }
  }
}
```

## Repeating Groups of Audiometry Exams

The *AudiometryList* exam makes it possible to run a group of audiometry exams.  The following options are available for
repeat logic to handle cases where one or more exams with the group do not successfully find a threshold:

* *repeatExamsThatFailedOnce* - Once all exams in the group have been run, if any failed, the failed exams will be repeated after a message
to the listener asking them to listen carefully.
* *getNotesIfAnyExamsFailedTwice* - Once all failed exams have been repeated once, if any repeats failed, show the listener a
message asking them to hand the tablet to the study administrator, and provide a notes text box for the administrator
to enter any relevant notes.

```json 
{
  "id": "AudiometryList",
  "title": "Left Ear List",
  "questionMainText": "Hughson-Westlake",
  "helpText": "Follow instructions",
  "instructionText": "This test measures your hearing sensitivity.  You will hear sounds at different pitches one ear at a time.  Your task is to tap the button when you hear a sound, no matter how soft the sound may be.",
  "responseArea": {
    "type": "chaAudiometryList",
    "repeatExamsThatFailedOnce": true,
    "getNotesIfAnyExamsFailedTwice": true,
    "presentationList": [
      {"F": 500},
      {"F": 1000},
      {"F": 2000, "Lstart": 50}
    ],
    "commonExamProperties": {
      "Lstart": 40,
      "UseSoftwareButton": true,
      "LevelUnits": "dB SPL",
      "OutputChannel": "HPL0"
    },
    "commonResponseAreaProperties": {
      "autoBegin": true,
      "autoSubmit": true
    }
  }
}
```

## CHA HINT

The HINT exam plays spoken words in noise.

```json
{
  "title":"A Simple CHA HINT Exam",
  "pages":[
    {
      "id": "HINT_1",
      "title": "Audiometry HINT Demo",
      "questionMainText": "HINT Demo",
      "helpText": "Follow instructions",
      "instructionText": "Select Appropriate Words",
      "responseArea": {
        "type": "chaHINT",
        "examProperties": {
          "Type": "military",
          "Direction": "left",
          "NoiseLevel": 50
        }
      }
    }
  ]
}
```
