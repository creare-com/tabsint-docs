---
id: introduction
title: Introduction
---

TabSINT is an open source platform for administering tablet based hearing-related exams, as well as general-purpose questionnaires. 
It is meant to be flexible, easy-to-use, and useful for administrators who manage studies of all sizes.

Exams and questionnaires are presented as a series of pages using a crisp and intuitive interface. 
Results are uploaded to a central server or saved locally on the tablet.

This software is &copy; Creare 2015-2021, released under the Apache v2 License provided in [LICENSE](https://gitlab.com/creare-com/tabsint/blob/master/LICENSE). License agreements for open source libraries used in the project are listed in [NOTICE](#notice).

## Releases

The latest TabSINT releases for Android are available on [Google Play](https://play.google.com/store/apps/details?id=com.creare.skhr.tabsint).

## Quick Start

To get started using TabSINT on a tablet, see the [TabSINT Quick Start](quick-start/tabsint). If you are interested in making changes to the TabSINT source code, see the [Developer Guide](https://gitlab.com/creare-com/tabsint/-/tree/develop/developer_guide/developer.md).

## How to Cite

Use the following citation to attribute TabSINT and WAHTS headset in publications:

- [Shapiro, M. L., Norris, J. A., Wilbur, J. C., Brungart D. S., & Clavier, O. H. (2020). *TabSINT: Open-Source Mobile Software for Distributed Studies of Hearing.* International Journal of Audiology, 59(sup1), S12-S19.](https://doi.org/10.1080/14992027.2019.1698776)

See [References](references/references) for more information.

## Notice

Code and documentation Copyright (C) 2015-2021 Creare. Code released under the Apache v2 License, provided in [LICENSE](https://gitlab.com/creare-com/tabsint/blob/master/LICENSE). All rights reserved.

Creare has used commercially reasonable efforts in preparing the
TabSINT Software but makes no guarantee or warranty of any nature
with regard to its use, performance, or operation.

Creare makes no representations or warranties, and Creare shall
incur no liability or other obligation of any nature whatsoever to
any person from any and all actions arising from the use of this
software.  THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
FOR A PARTICULAR PURPOSE ARE HEREBY EXPRESSLY EXCLUDED. The final
responsibility for the proper use and functioning of the TabSINT
Software shall rest solely with the USAMRAA.

### Acknowledgment

This code was developed and maintained with funding support from the US Army Medical Research Materiel Command and the Army Public Health Command under SBIR Phase III Awards #W81XWH-13-C-0194, W81XWH-16-C-0160, W81XWH-17-C-0218, W81XWH-19-C-0175, W81XWH-20-C-0070, W81XWH-21-C-0082 to [Creare LLC](https://www.creare.com). In particular, we gratefully acknowledge the support and contributions of the Audiology and Speech Center at the Walter Reed National Military Medical Center and the Department of Defense Hearing Center of Excellence in the development and extensive testing of this software.

### Privacy Policy

TabSINT is a platform for configuring and administering hearing-related exams, as well as general-purpose questionnaires. As a flexible platform, TabSINT may be configured to automatically collect many types of data, including images, audio, and tablet location. This data can be uploaded to a back end data repository only if configured by a TabSINT Administrator. The TabSINT administrator is responsible for configuring the application, developing and testing the protocol, and managing test results. Data is never transferred outside the application unless configured by the TabSINT administrator, or if TabSINT is configured to upload application logs for debugging purposes.

#### Logging

The TabSINT application includes a setting to upload application logs to private server to help debug remote issues. By default, this setting is disabled.  If this setting is enabled, the application may record device state, location, network status, or test results in the application log and upload this data to our server. Please ensure logging is set to the desired state before using the application. 

If you have questions regarding these policies, please contact tabsint@creare.com.


### Third-Party Licenses

The TabSINT Software relies on many open source libraries.
We recommend you read their licenses, as their terms may differ from the terms described in our [LICENSE](https://gitlab.com/creare-com/tabsint/blob/master/LICENSE):

* AngularJS ([MIT license](https://github.com/angular/angular.js/blob/master/LICENSE))
* Cordova  ([Apache V2 License ](https://github.com/apache/cordova-android/blob/master/LICENSE))
* jquery ([License](https://github.com/jquery/jquery/blob/master/LICENSE.txt))
* Angular-UI ([MIT](https://github.com/angular-ui/bootstrap/blob/master/LICENSE))
* Bootstrap ([MIT](https://github.com/twbs/bootstrap/blob/master/LICENSE))
* Lodash ([MIT](https://raw.githubusercontent.com/lodash/lodash/4.12.0/LICENSE))
* RequireJs ([BSD, MIT](https://github.com/jrburke/requirejs/blob/master/LICENSE))
* d3 ([License](https://github.com/mbostock/d3/blob/master/LICENSE))
* ngStorage ([MIT](https://github.com/gsklee/ngStorage/blob/master/LICENSE))
* ZXing barcode scanning library ([Apache V2](https://github.com/zxing/zxing/wiki/License-Questions))
* Gitbook ([Apache V2 License](https://github.com/GitbookIO/gitbook/blob/master/LICENSE))
* CryptoJS ([MIT](https://github.com/brix/crypto-js/blob/develop/LICENSE))

Additional files included in `/node_modules`, `/www/bower_components`, `/plugins`, and `tabsint_plugins` directories are externally maintained libraries used by this software, which have their own licenses. 
