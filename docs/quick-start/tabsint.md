---
id: tabsint
title: TabSINT Quick Start Guide
sidebar_label: TabSINT Quick Start
---

This section describes how to install and launch the TabSINT software.

## Download TabSINT

- On your tablet:
	- Open the Google Play Store and search for `TabSINT`, or 
	- Navigate to `https://play.google.com/store` in the web browser and then search for `TabSINT`

- Click `Install`

<div style="text-align:center"><img src="../assets/google-play.png" width="50%"/></div>

- To download older versions of TabSINT, see the [Download Older Versions of TabSINT](#download-older-versions-of-tabsint) section below.
 
## Starting TabSINT

- Once installed, click `Open` to launch from the Google Play Store, or select the TabSINT icon from the tablet applications

 <div style="text-align:center"><img src="../assets/download-google-play-open.png" width="100%"/></div>

- To begin configuring TabSINT, select **Admin View** and enter the **PIN** 7114. 

<div style="text-align:center"><img src="../assets/pin.png" width="50%"/></div>

- This PIN can be changed by  clicking on **Show Advanced Settings** in the TabSINT section of the Setup tab and clicking on the pencil icon next to the Admin PIN.

<div style="text-align:center"><img src="../assets/pin-change.png" width="100%"/></div>

- For more information on configuring TabSINT, loading protocols, administering exams, viewing results and more, visit the [Configuration](../user-guide/configuration) page of this documentation.

## Download Older Versions of TabSINT

To download an older version of TabSINT:
- From your tablet, navigate to the [TabSINT Repository Releases page](https://gitlab.com/creare-com/tabsint/-/releases)
- Download and then open the desired **tabsint.apk**
- Follow the prompts to install

<div style="text-align:center"><img src="../assets/apk.png" width="50%"/></div>

