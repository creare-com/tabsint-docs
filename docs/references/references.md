---
id: references
title: References
sidebar_label: References
---

## How to Cite

Use the following citation to attribute TabSINT and WAHTS headset in publications:

- [Shapiro, M. L., Norris, J. A., Wilbur, J. C., Brungart D. S., & Clavier, O. H. (2020). *TabSINT: Open-Source Mobile Software for Distributed Studies of Hearing.* International Journal of Audiology, 59(sup1), S12-S19.](https://doi.org/10.1080/14992027.2019.1698776)

## TabSINT

- [2017 Internet & Audiology Presentation](/references/2017-Internet-Audiology/index.html)
- [2016 IHCON Poster](/references/2016-IHCON.pdf)
- [2016 American Auditory Society Presentation](/references/2016-AAS/index.html)

## TabSINT Protocols

- Protocol Workshop #1
	- [Presentation](/references/Protocol-Training-Workshop-Session-1.pdf) 
	- [Video](https://youtu.be/zQ8gBDBRCQ4)
- Protocol Workshop #2
	- [Presentation](/references/Protocol-Training-Workshop-Session-2.pdf)
	- [Video (Part 1)](https://youtu.be/be6y_MRv2vo)
	- [Video (Part 2)](https://youtu.be/0-CrKmy5yas)
- Protocol Workshop #3
	- [Presentation](/references/Protocol-Training-Workshop-Session-3.pdf)
	- [Video](https://youtu.be/JnSf2Svf5zs)
- Protocol Workshop #4
	- [Presentation](/references/Protocol-Training-Workshop-Session-4.pdf)
	- [Video](https://youtu.be/AoVyfq4kvTo)

## Wireless Automated Hearing-Test System (WAHTS)

### 2022
- [Bessen, S. Y., Magro, I. L., Alvarez, K. M., Cowan, D. R., Peñalba, D., Fellows, A., Gonzalez-Quiroz, M., Rieke, C., Buckey, J.C., Niemczak, C. & Saunders, J. E. (2022). "Test-Retest repeatability of automated threshold audiometry in Nicaraguan schoolchildren." International Journal of Audiology, 1-8.](https://doi.org/10.1080/14992027.2022.2032416)
- [Clavier, O.H., Norris, J.A., Hinckley Jr, D.W., Martin, W.H., Lee, S.Y., Soli, S.D., Brungart, D.S., Schurman, J.R., Larsen, E., Mehraei, G. and Quigley, T.M. (2022). "Reference equivalent threshold sound pressure levels for the Wireless Automated Hearing Test System." The Journal of the Acoustical Society of America, 152(1), 601-608.](https://doi.org/10.1121/10.0012733)
- [Kulinski, D., Dirks, C., Carr W., Sheffield, B., Kamimori, G., & Brungart, D. (2022). "Field assessment of acute auditory responses to environmental exposures in close quarters tactics training." International Journal of Audiology](https://doi.org/10.1080/14992027.2022.2028023)
- [Kulinski, D., & Brungart, D. S. (2022). "Using a boothless audiometer to estimate personal attenuation rating in a military hearing conservation clinic." JASA Express Letters, 2(4), 043601.](https://doi.org/10.1121/10.0010108)

### 2021
- [Barr, L. (2021). "Mobile hearing test system enables quicker diagnosis, treatment."](https://health.mil/News/Articles/2021/07/08/Mobile-hearing-test-system-enables-quicker-diagnosis-treatment)
- [Behar, A. (2021). "Audiometric Tests without Booths." International Journal of Environmental Research and Public Health, 18(6), 3073.](https://doi.org/10.3390/ijerph18063073)
- [Gates, K., Hecht, Q. A., Grantham, M. A., Fallon, A. J., & Martukovich, M. (2021). "Hearing Health Care Delivery Outside the Booth." Perspectives of the ASHA Special Interest Groups (2021): 1-14.](https://doi.org/10.1044/2021_PERSP-20-00264)
- [JDE Lee, DM Bowley, J Miles, J Muzaffar, R Poole, LE Orr. "The 'Downrange Acoustic Toolbox': An Active Solution for Acute Acoustic Trauma." Otology, Neuro-Otology and Audio-Vestibular Medicine. Poster Number PO-2011.](https://www.postersessiononline.eu/173580348_eu/congresos/BACO2021/aula/-PO_2011_BACO2021.pdf)

### 2020
- [Haro S, Smalt CJ, Ciccarelli GA, Quatieri TF. Deep Neural Network Model of Hearing-Impaired Speech-in-Noise Perception. Front Neurosci. 2020;14:588448. Published 2020 Dec 15.](https://doi.org/10.3389/fnins.2020.588448)
- [Kulinski, D., Makashay, M. J., Dirks, C., Sheffield, B., & Brungart, D. S. (2020). Development of a tablet-based fit test system for military and austere environments. The Journal of the Acoustical Society of America, 148(4), 2805-2805.](https://doi.org/10.1121/1.5147810)
- [Magro, Isabelle, Odile Clavier, Karen Mojica, Catherine Rieke, Eric Eisen, Debra Fried, Anita Stein-Meyers, Abigail Fellows, Jay Buckey, and James Saunders. "Reliability of Tablet-based Hearing Testing in Nicaraguan Schoolchildren: A Detailed Analysis." Otology & Neurotology 41, no. 3 (2020): 299-307.](https://journals.lww.com/otology-neurotology/Abstract/2020/03000/Reliability_of_Tablet_based_Hearing_Testing_in.4.aspx)
- [Military Health System Communications Office (2019). "Portable system demonstrates capability to save hearing downrange"](https://health.mil/News/Articles/2020/05/13/Portable-system-demonstrates-capability-to-save-hearing-downrange)

### 2019
- [2019 ARO Poster](/references/TN-1096.pdf)
- [Hecht, Quintin A. and Hammill, Tanisha L. and Calamia, Paul T. and Smalt, Christopher J. and Brungart, Douglas S. (2019). "Characterization of acute hearing changes in United States military populations." The Journal of the Acoustical Society of America, 146(5), 3839-3848.](https://doi.org/10.1121/1.5132710)
- [Ruths, J. (2019). Application of the Wireless Audiometric Testing System at a Refugee Center with a Multilingual Population. Capstone Project.](https://digscholarship.unco.edu/capstones/52/)
- [Stumpf, A. (2019). Comparison of Automated Hearing Testing Approaches for Outpatients Receiving Ototoxic Chemotherapy. Capstone Project.](https://digscholarship.unco.edu/cgi/viewcontent.cgi?article=1133&context=capstones)

### 2017
- [2017 Internet & Audiology Poster](/references/2017-Internet-Audiology-WAHTS.pdf)
- [Brungart, D., Schurman, J., Konrad-Martin, D., Watts, K., Buckey, J., Clavier, O., ... Dille, M. F. (2017). Using tablet-based technology to deliver time-efficient ototoxicity monitoring. International Journal of Audiology, 1-9.](https://www.tandfonline.com/doi/abs/10.1080/14992027.2017.1370138?journalCode=iija20)
- [Meinke, D. K., Norris, J. A., Flynn, B. P., & Clavier, O. H. (2017). Going wireless and booth-less for hearing testing in industry. *International Journal of Audiology*, 56(sup1), 41-51.](https://doi.org/10.1080/14992027.2016.1261189)
- [Rieke, Catherine C., Odile H. Clavier, Lindsay V. Allen, Allison P. Anderson, Chris A. Brooks, Abigail M. Fellows, Douglas S. Brungart, and Jay C. Buckey. "Fixed-level frequency threshold testing for ototoxicity monitoring." Ear and hearing 38, no. 6 (2017): e369-e375.](https://journals.lww.com/ear-hearing/pages/articleviewer.aspx?year=2017&issue=11000&article=00021&type=Fulltext)

### 2016
- [2016 NHCA Presentation](/references/NHCA_2016_-_Going_wireless_and_booth-less_for_hearing_testing_in_industry_-_final.pdf)
- [2016 World Congress of Audiology Presentation](/references/WCA2016-Final.pdf)
